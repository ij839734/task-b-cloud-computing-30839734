# -*- coding: utf-8 -*-
"""Task B cloud computing 30839734.py

"""

import csv
import time
import multiprocessing as mp

def mapper(row):
    return (row[0], 1)

def reducer(item):
    passenger_id, counts = item
    return passenger_id, sum(counts)

if __name__ == '__main__':
    # Read data from CSV file
    with open('/content/AComp_Passenger_data_no_error.csv') as f:
        reader = csv.reader(f)
        data = list(reader)

    # Remove duplicates
    data = list(set(tuple(row) for row in data))

    # Start timer
    start_time = time.time()

    # Determine number of CPUs and chunk size for multiprocessing
    num_cpus = mp.cpu_count()
    chunk_size = len(data) // num_cpus

    # Map Phase
    with mp.Pool() as p:
        results = p.map(mapper, data, chunksize=chunk_size)

    # Reduce Phase
    counts = {}
    for passenger_id, count in results:
        if passenger_id in counts:
            counts[passenger_id].append(count)
        else:
            counts[passenger_id] = [count]

    # Find max count
    max_count = 0
    max_passenger_ids = []
    for passenger_id, counts in counts.items():
        total_count = sum(counts)
        if total_count > max_count:
            max_count = total_count
            max_passenger_ids = [passenger_id]
        elif total_count == max_count:
            max_passenger_ids.append(passenger_id)

    # Print results in tabular format
    print("{:<20} {:<20}".format("Passenger ID", "Number of Flights"))
    print("-" * 40)
    for passenger_id in max_passenger_ids:
        print("{:<20} {:<20}".format(passenger_id, max_count))
    print()

    # Print additional information
    print("Total Records:", len(data))
    print("Number of CPUs:", num_cpus)
    print("Chunk Size:", chunk_size)
    print("Completed Time:", time.time() - start_time)